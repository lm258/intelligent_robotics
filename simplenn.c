#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>


#define NUMBER_OF_INPUTS 2
#define NUMBER_OF_HIDDEN_NODES 2
#define NUMBER_OF_OUTPUTS 1
#define BIAS 1

struct HiddenNode {
		double weights[NUMBER_OF_INPUTS + 1];
		double output;
};

struct OutputNode {
		double weights[NUMBER_OF_HIDDEN_NODES + 1];
		double output;
};

int inputs[NUMBER_OF_INPUTS];
struct HiddenNode hiddenLayer[NUMBER_OF_HIDDEN_NODES];
struct OutputNode outputLayer[NUMBER_OF_OUTPUTS];

double hiddenLayerOutputs[NUMBER_OF_HIDDEN_NODES];
double outputLayerOutputs[NUMBER_OF_OUTPUTS];


/* Utility Functions
***************************************************** */
double uniform_random_number() {
		double dr;
		int r;

		r = rand();
		dr = (double)(r)/(double)(RAND_MAX);
		return(dr);
}

// gaussian random number with mean 0 and standard deviation 1:
double grand(void)
{ double x1, x2, y1, pi;

 pi = acos(-1.0);

//	 y1 = sqrt( - 2 ln(x1) ) cos( 2 pi x2 )
//         y2 = sqrt( - 2 ln(x1) ) sin( 2 pi x2 )

x1 = uniform_random_number();
 x2 = uniform_random_number();
 y1 = sqrt( -2.0 * log(x1)) * cos(2.0 * pi * x2);
return y1;
 }

/* Print Network
***************************************************** */
void printAllWeights() {
		int nodeIndex, inputIndex;
		printf("Weights: ");
		for(nodeIndex=0; nodeIndex<NUMBER_OF_HIDDEN_NODES; nodeIndex++) {
				for(inputIndex=0; inputIndex<NUMBER_OF_INPUTS+BIAS; inputIndex++) {
						printf("%f ", hiddenLayer[nodeIndex].weights[inputIndex]);
				}
		}
		for(nodeIndex=0; nodeIndex<NUMBER_OF_OUTPUTS; nodeIndex++) {
				for(inputIndex=0; inputIndex<NUMBER_OF_HIDDEN_NODES+BIAS; inputIndex++) {
						printf("%f ", outputLayer[nodeIndex].weights[inputIndex]);
				}
		}
		printf("\n");
}

void printInputs() {
		int i;
		printf("NETWORK INPUTS:\n");
				for(i=0; i<NUMBER_OF_INPUTS; i++) {
						printf("Input %d: %d \n", i, inputs[i]);
				}
		printf("\n");
}

void printHiddenLayer() {
		int nodeIndex, inputIndex;
		printf("HIDDEN LAYER:\n");
		for(nodeIndex=0; nodeIndex<NUMBER_OF_HIDDEN_NODES; nodeIndex++) {
				printf("Address of Node %d: %d\n", nodeIndex, &hiddenLayer[nodeIndex]);
				for(inputIndex=0; inputIndex < NUMBER_OF_INPUTS+1; inputIndex++) {
						if(inputIndex == 0)
								printf("\tWeight of Node %d for Input %d (bias): \t%f\n", nodeIndex, inputIndex, hiddenLayer[nodeIndex].weights[inputIndex]);
						else
								printf("\tWeight of Node %d for Input %d: \t\t%f\n", nodeIndex, inputIndex, hiddenLayer[nodeIndex].weights[inputIndex]);
				}
				printf("\t\tOutput of Node %d: %f \n", nodeIndex, hiddenLayerOutputs[nodeIndex]);
		}
		printf("\n");
}

void printOutputLayer() {
		int nodeIndex, inputIndex;
		printf("OUTPUT LAYER:\n");
		for(nodeIndex=0; nodeIndex<NUMBER_OF_OUTPUTS; nodeIndex++) {
				printf("Address of Node %d: %d\n", nodeIndex, &outputLayer[nodeIndex]);
				for(inputIndex=0; inputIndex < NUMBER_OF_HIDDEN_NODES+1; inputIndex++) {
						if(inputIndex == 0)
								printf("\tWeight of Node %d for Input %d (bias): \t%f\n", nodeIndex, inputIndex, outputLayer[nodeIndex].weights[inputIndex]);
						else
								printf("\tWeight of Node %d for Input %d: \t\t%f\n", nodeIndex, inputIndex, outputLayer[nodeIndex].weights[inputIndex]);
				}
				printf("\t\tOutput of Node %d: %f \n", nodeIndex, outputLayerOutputs[nodeIndex]);
		}
		printf("\n");
}

void printNetwork() {
		printInputs();
		printHiddenLayer();
		printOutputLayer();
}


/* Create Layers
***************************************************** */
void setNewOutputNodeWeights(struct OutputNode *newNode) {
		int i;
		for(i = 0; i < (NUMBER_OF_HIDDEN_NODES + 1); i++) {
				newNode->weights[i] = uniform_random_number();
		}
}

struct OutputNode createNewOutputNode() {
		struct OutputNode newNode;
		setNewOutputNodeWeights(&newNode);
		return newNode;
}

void createOutputLayer() {
		int i;
		for(i=0; i<NUMBER_OF_OUTPUTS; i++) {
				outputLayer[i] = createNewOutputNode();
		}
}

void setNewHiddenNodeWeights(struct HiddenNode *newNode) {
		int i;
		for(i = 0; i < (NUMBER_OF_INPUTS + 1); i++) {
				newNode->weights[i] = uniform_random_number();
		}
}

struct HiddenNode createNewHiddenNode() {
		struct HiddenNode newNode;
		setNewHiddenNodeWeights(&newNode);
		return newNode;
}

void createHiddenLayer() {
		int i;
		for(i=0; i<NUMBER_OF_HIDDEN_NODES; i++) {
				hiddenLayer[i] = createNewHiddenNode();
		}
}

/* Calculate Outputs
***************************************************** */
double activationFunction(double sumOfNodeInputs) {
	// Fast sigmoid
	return sumOfNodeInputs / (1 + fabs((float)sumOfNodeInputs));
}

double sumHiddenNodeInputs(struct HiddenNode *node) {
		int inputIndex;
		double inputSum = 0.0;
		
		inputSum += node->weights[0] * 1;  // add in bias
		
		for(inputIndex=1; inputIndex<NUMBER_OF_INPUTS+1; inputIndex++) {
				inputSum += node->weights[inputIndex] * inputs[inputIndex-1];
		}
		
		return inputSum;
}

double sumOutputNodeInputs(struct OutputNode *node) {
		int inputIndex;
		double inputSum = 0.0;
		
		inputSum = node->weights[0] * 1;  // add in bias
		
		for(inputIndex=1; inputIndex<NUMBER_OF_HIDDEN_NODES+1; inputIndex++) {
				inputSum += (node->weights[inputIndex] * hiddenLayerOutputs[inputIndex-1]);
		}

		return inputSum;
}

double getHiddenNodeOutput(struct HiddenNode *node) {
		return activationFunction(sumHiddenNodeInputs(node));
}

double getOutputNodeOutput(struct OutputNode *node) {
		return activationFunction(sumOutputNodeInputs(node));
}

void calculateHiddenLayerOutputs() {
		int i;
		for(i=0; i<NUMBER_OF_HIDDEN_NODES; i++) {
				hiddenLayerOutputs[i] = getHiddenNodeOutput(&hiddenLayer[i]);
		}
}

void calculateOutputLayerOutputs() {
		int i;
		for(i=0; i<NUMBER_OF_OUTPUTS; i++) {
				outputLayerOutputs[i] = getOutputNodeOutput(&outputLayer[i]);
		}
}

void runNetwork() {
		calculateHiddenLayerOutputs();
		calculateOutputLayerOutputs();
}

/***************************************************** */
/* GENETIC ALGORITHM
***************************************************** */

/* Get population
***************************************************** */
#define SIZE_OF_POPULATION 50
#define GENOME_LENGTH (NUMBER_OF_INPUTS+1)*NUMBER_OF_HIDDEN_NODES+(NUMBER_OF_HIDDEN_NODES+1)*NUMBER_OF_OUTPUTS

int TOURNAMENT_SELECTION_NUMBER;

struct Individual {
		double genome[GENOME_LENGTH];
		double fitness;
};

struct Individual population[SIZE_OF_POPULATION];

struct Individual child;
double parent1_genome[GENOME_LENGTH];
double parent2_genome[GENOME_LENGTH];


void mapGenomeToWeights(double genome[GENOME_LENGTH]) {
		int nodeIndex, inputIndex;
		int count = 0;
		for(nodeIndex=0; nodeIndex<NUMBER_OF_HIDDEN_NODES; nodeIndex++) {
			for(inputIndex=0; inputIndex<NUMBER_OF_INPUTS+BIAS; inputIndex++) {
				hiddenLayer[nodeIndex].weights[inputIndex] = genome[count];
				count += 1;
			}
		}
		for(nodeIndex=0; nodeIndex<NUMBER_OF_OUTPUTS; nodeIndex++) {
				for(inputIndex=0; inputIndex<NUMBER_OF_HIDDEN_NODES+BIAS; inputIndex++) {
						outputLayer[nodeIndex].weights[inputIndex] = genome[count];
						count += 1;
				}
		}
}

void printPopulation() {
		printf("\n\nPOPULATION: \n");
		int i;
		for(i=0; i<SIZE_OF_POPULATION; i++) {
				printf("%d ", i);
				mapGenomeToWeights(population[i].genome);
				printf("Fitness: %f ", population[i].fitness);
				printAllWeights();
		}
}

/* test function, requires two inputs and one output */
double mean_squared_error_for_XOR(int doPrint) {
		double squared_error = 0.0;
		double output;
		
		int dataset[4][3] = {
				{0, 0, 0},
				{0, 1, 1},
				{1, 0, 1},
				{1, 1, 0}
		};
		
		int rows_in_dataset = 4;
		
		int rows;
		for(rows=0; rows<rows_in_dataset; rows++) {
				inputs[0] = dataset[rows][0];
				inputs[1] = dataset[rows][1];
				runNetwork();
				if(doPrint == 1)
					printf("%d %d %f \t", dataset[rows][0], dataset[rows][1], outputLayerOutputs[0]);
				squared_error += (outputLayerOutputs[0] - dataset[rows][2]) * (outputLayerOutputs[0] - dataset[rows][2]);
		}
		
		return (squared_error / 4.0) * -1.0;
}

double evaluate_individual_fitness(struct Individual *testIndividual) {
		mapGenomeToWeights(testIndividual->genome);
		
		// for test purposes, use mean squared error (* -1)
		return mean_squared_error_for_XOR(0);
}

struct Individual createIndividual() {
		struct Individual new_individual;
		
		int i;
		for(i=0; i<GENOME_LENGTH; i++) {
				new_individual.genome[i] = uniform_random_number();
		}
		new_individual.fitness = evaluate_individual_fitness(&new_individual);
		
		return new_individual;
}

void createPopulation() {
		int i;
		for(i=0; i<SIZE_OF_POPULATION; i++) {
				population[i] = createIndividual();
		}
}

void tournament_selection(double parent[GENOME_LENGTH]) {
		int i, random_index, best_tournament_candidate_index, new_candidate_index;
    best_tournament_candidate_index = rand() % SIZE_OF_POPULATION;
		for(i=0; i<TOURNAMENT_SELECTION_NUMBER; i++) {
				new_candidate_index = rand() % SIZE_OF_POPULATION;
				if(population[new_candidate_index].fitness > population[best_tournament_candidate_index].fitness)
						best_tournament_candidate_index = new_candidate_index;
		}
		
		// set parent genome
		for(i=0; i<GENOME_LENGTH; i++) {
				parent[i] = population[best_tournament_candidate_index].genome[i];
		}		
}

void mutate_child_genome() {
		int index, rand_index;
		int mutation_genes = 2; // number of genes to mutate
		
		// choose random genes and mutate
		for(index=0; index<mutation_genes; index++) {
				int rand_index = (int)(uniform_random_number() * (GENOME_LENGTH * 1.0));
				child.genome[rand_index] += grand();
		}
}

void print_child_genome() {
		int i;
		printf("Child genome: ");
		for(i=0; i<GENOME_LENGTH; i++) {
				printf("%f ", child.genome[i]);
		}
		printf("\n");
}

void swap_worst_individual_for_new_child(int worst_individual_index) {
		int i;
		for(i=0; i<GENOME_LENGTH; i++) {
				population[worst_individual_index].genome[i] = child.genome[i];
		}
		population[worst_individual_index].fitness = child.fitness;
}

double best_fitness() {
		int i;
		int best_individual_index = 0;
		for(i=0; i<SIZE_OF_POPULATION; i++) {
				if(population[i].fitness > population[best_individual_index].fitness)
						best_individual_index = i;
		}
		return population[best_individual_index].fitness;
}

void update_population() {
		int i;
		int best_individual_index = 0;
		int worst_individual_index = 0;
		for(i=0; i<SIZE_OF_POPULATION; i++) {
				if(population[i].fitness > population[best_individual_index].fitness)
						best_individual_index = i;
				if(population[i].fitness < population[worst_individual_index].fitness)
						worst_individual_index = i;
		}
		printf("Best individual fitness: %f\n", population[best_individual_index].fitness); 
		
		if(population[worst_individual_index].fitness <= child.fitness)
				swap_worst_individual_for_new_child(worst_individual_index);
}
		
int crossover(void) {
		//Two-point crossover
		int i;
		int crossover_point1 = (int)(uniform_random_number() * (GENOME_LENGTH * 1.0));
		int crossover_point2 = (int)((((GENOME_LENGTH - crossover_point1) * 1.0) * uniform_random_number()) + crossover_point1);
		for(i=0; i<GENOME_LENGTH; i++) {
				if(i<crossover_point1 || i>crossover_point2)
						child.genome[i] = parent1_genome[i];
				else
						child.genome[i] = parent2_genome[i];
		}
}

void run_evolution_step(double mutation_rate, double crossover_rate) {
		// select parent 1		
		tournament_selection(parent1_genome);
		
		// set child genome as a copy of parent 1
		int i;
		for(i=0; i<GENOME_LENGTH; i++) {
				child.genome[i] = parent1_genome[i];
		}
		
		//crossover
		if(uniform_random_number() < crossover_rate) {
				tournament_selection(parent2_genome);
				crossover();
		}
		
		//mutate child
		if(uniform_random_number() < mutation_rate) {
				mutate_child_genome();
		}
		
		//evaluate child fitness
		child.fitness = evaluate_individual_fitness(&child);
		
		update_population();
}

void evolve(int generations, double mutation_rate, double crossover_rate) {
		int i;
		for(i=0; i<generations; i++) {
				printf("%d ", i);
				run_evolution_step(mutation_rate, crossover_rate);
		}
}

void setup_evolvable_network() {
		createHiddenLayer();
		createOutputLayer();
		createPopulation();
}

 
/* Main
***************************************************** */
int main() {
		// Set up variables
		long int seed = 1523;
		int generations = 10000;
		double mutation_rate = 0.9;
		double crossover_rate = 0.9;
		TOURNAMENT_SELECTION_NUMBER = 5;
		
		// Set up timer
		clock_t begin, end;
		double time_spent;
		begin = clock();

		// Evolve network
		srand(seed);
		setup_evolvable_network();
		evolve(generations, mutation_rate, crossover_rate);

		// Best result
		mean_squared_error_for_XOR(1);
		
		// Print timer
		end = clock();
		time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
		printf("\nTime spent: %f", time_spent);
}