#include <stdio.h>
#include "ann.h"

/*Create some little arrays to store our neural network in*/
float weights[GENOME_LENGTH] = {0};
float input[NETWORK_HEIGHT]  = {0};
float output[NETWORK_HEIGHT] = {0};

/**
 * This is an example.
 * Just sets the first two inputs then computes the output.
 * Two input nodes are simply {1} {2}
 * Connected to the output nodes.
*/
int main() {
    int i;

    /*Set example input data*/
    input[0] = 1.0f;
    input[1] = 2.0f;

    /*Set the sample weights for our input*/
    for(i=0; i<GENOME_LENGTH; i++){
        weights[i] = 1.0f;
    }

    /*Print inputs*/
    for(i=0;i<NETWORK_HEIGHT; i++) {
        printf( "input: %f\n", input[i] );
    }

    ann_calculate( weights, input, output);

    /*Print outputs*/
    for(i=0;i<NETWORK_HEIGHT; i++) {
        printf( "output: %f\n", output[i] );
    }

    return 0;
}