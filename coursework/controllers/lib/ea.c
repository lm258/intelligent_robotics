#include "ea.h"

static struct Individual child;
static double parent1_genome[GENOME_LENGTH];
static double parent2_genome[GENOME_LENGTH];

double uniform_random_number(){
    return (double)rand()/(double)RAND_MAX;
}

double grand() {
    double x1, x2, y1, pi;
    pi = acos(-1.0);

    x1 = uniform_random_number();
    x2 = uniform_random_number();
    y1 = sqrt( -2.0 * log(x1)) * cos(2.0 * pi * x2);
    return y1;
}

/**
 * Create a new individual for our population
 * returns the new individual
*/
Individual ea_create_individual() {
    Individual new_individual;
    
    int i;
    for(i=0; i<GENOME_LENGTH; i++) {
        new_individual.genome[i] = uniform_random_number();
    }
    new_individual.fitness = ea_evaluate_individual_fitness(&new_individual);

    return new_individual;
}

/**
 * Create the population for our ANN
 * store the individuals in population array
*/
void ea_create_population() {
    int i;
    for(i=0; i<SIZE_OF_POPULATION; i++) {
        printf("new individual: %d\n", i);
        population[i] = ea_create_individual();
    }
}

void ea_tournament_selection(double parent[GENOME_LENGTH]) {
    int i, best_tournament_candidate_index, new_candidate_index;
    best_tournament_candidate_index = rand() % SIZE_OF_POPULATION;

    for(i=0; i<TOURNAMENT_SELECTION_NUMBER; i++) {
        new_candidate_index = rand() % SIZE_OF_POPULATION;
        if(population[new_candidate_index].fitness > population[best_tournament_candidate_index].fitness){
            best_tournament_candidate_index = new_candidate_index;
        }
    }
    
    // set parent genome
    for(i=0; i<GENOME_LENGTH; i++) {
        parent[i] = population[best_tournament_candidate_index].genome[i];
    }   
}

void ea_mutate_child_genome() {
    int index, rand_index;
    int mutation_genes = 2; // number of genes to mutate
    
    // choose random genes and mutate
    for(index=0; index<mutation_genes; index++) {
        rand_index = (int)(uniform_random_number() * (GENOME_LENGTH * 1.0));
        child.genome[rand_index] += grand();
    }
}

void ea_fprint_genotype(Individual * genotype, FILE* file){
    fprintf(file, "FITNESS: %lf GENOME: ", genotype->fitness);
    int i;
    for(i=0; i<GENOME_LENGTH; i++) {
        fprintf(file, "%lf ", genotype->genome[i]); 
    }
    fprintf(file, "\n");
}

void ea_print_child_genome() {
    int i;
    printf("Child genome: ");
    for(i=0; i<GENOME_LENGTH; i++) {
        printf("%f ", child.genome[i]);
    }
    printf("\n");
}

void ea_swap_worst_individual_for_new_child(int worst_individual_index) {
    int i;
    for(i=0; i<GENOME_LENGTH; i++) {
        population[worst_individual_index].genome[i] = child.genome[i];
    }
    population[worst_individual_index].fitness = child.fitness;
}

double ea_best_fitness() {
    int i;
    int best_individual_index = 0;
    for(i=0; i<SIZE_OF_POPULATION; i++) {
        if(population[i].fitness > population[best_individual_index].fitness){
            best_individual_index = i;
        }
    }
    return population[best_individual_index].fitness;
}

void ea_update_population() {
    int i;
    int best_individual_index = 0;
    int worst_individual_index = 0;
    for(i=0; i<SIZE_OF_POPULATION; i++) {
        if(population[i].fitness > population[best_individual_index].fitness){
            best_individual_index = i;
        }
        if(population[i].fitness < population[worst_individual_index].fitness){
            worst_individual_index = i;
        }
    }
    printf("Best individual fitness: %f\n", population[best_individual_index].fitness);
    
    if(population[worst_individual_index].fitness <= child.fitness){
        ea_swap_worst_individual_for_new_child(worst_individual_index);
    }
}
        
void ea_crossover() {
    //Two-point crossover
    int i;
    int crossover_point1 = (int)(uniform_random_number() * (GENOME_LENGTH * 1.0));
    int crossover_point2 = (int)((((GENOME_LENGTH - crossover_point1) * 1.0) * uniform_random_number()) + crossover_point1);
    for(i=0; i<GENOME_LENGTH; i++) {
        if(i<crossover_point1 || i>crossover_point2){
            child.genome[i] = parent1_genome[i];
        } else {
            child.genome[i] = parent2_genome[i];
        }
    }
}

void ea_run_evolution_step(double mutation_rate, double crossover_rate) {
    // select parent 1      
    ea_tournament_selection(parent1_genome);
    
    // set child genome as a copy of parent 1
    int i;
    for(i=0; i<GENOME_LENGTH; i++) {
        child.genome[i] = parent1_genome[i];
    }
    
    //crossover
    if(uniform_random_number() < crossover_rate) {
        ea_tournament_selection(parent2_genome);
        ea_crossover();
    }
    
    //mutate child
    if(uniform_random_number() < mutation_rate) {
        ea_mutate_child_genome();
    }
    
    //evaluate child fitness
    child.fitness = ea_evaluate_individual_fitness( &child );
    
    ea_update_population();
}

void ea_evolve(int generations, double mutation_rate, double crossover_rate) {
    int i;
    for(i=0; i<generations; i++) {
        printf("Generation: %d\n", i);
        ea_run_evolution_step(mutation_rate, crossover_rate);
    }
}