#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "ann.h"

/***************************************************** */
/* GENETIC ALGORITHM
***************************************************** */

/* Get population
***************************************************** */
#define SIZE_OF_POPULATION 300
#define TOURNAMENT_SELECTION_NUMBER 50

struct Individual {
    double genome[GENOME_LENGTH];
    double fitness;
};
typedef struct Individual Individual;
Individual population[SIZE_OF_POPULATION];


/**
 * Create the population for our ANN
 * store the individuals in population array
*/
void ea_create_population();

/**
 * Evaluate fitness of this indivual set of weights
 * struct * Individual
 * returns double Fitness of individual
*/
double ea_evaluate_individual_fitness(Individual * individual);

void ea_evolve(int generations, double mutation_rate, double crossover_rate);

void ea_fprint_genotype(Individual * genotype, FILE* file);