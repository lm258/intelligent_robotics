/**
 * (c) 2014 Lewis Maitland
 * This program is free software; any publications presenting results
 * obtained with this program must mention it and its origin. You 
 * can redistribute it and/or modify it under the terms of the GNU 
 * General Public License as published by the Free Software 
 * Foundation; either version 2 of the License, or (at your option) 
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
*/
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#ifndef _ANN_H_
#define _ANN_H_

/*Set to 1 for debug mode (prints calculations/ disabled activation for simplicity)*/
#define DEBUG 0

/*Network height - IE number of input neurons*/
#define NETWORK_HEIGHT 13 //Y
#define NETWORK_DEPTH NETWORK_HEIGHT //Z

/*This is the width X of the network*/
#define NETWORK_WIDTH 2

/*Genome length is the size of the neural network*/
#define GENOME_LENGTH NETWORK_WIDTH*NETWORK_DEPTH*NETWORK_HEIGHT

/**
 * Translates 3d coord into a 1d array
*/
uint8_t as3D(uint8_t x, uint8_t y, uint8_t z);

/**
 * This function will initalize the
 * neural network with random values.
 * double * network - The weights of the network
*/
void ann_init_network(double * network);

/**
 * This function is used to
 * calculate the ouput of the neural
 * network.
 * double * network - The weights of the network
 * double * input   - The input
 * double * output  - The output
*/
void ann_calculate( double * network, double * input, double * output );


/**
 * Print the artificial neural network
 * double * network - The weights of the network
*/
void ann_print( double * network );

#endif