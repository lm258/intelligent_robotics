/**
 * (c) 2014 Lewis Maitland
 * This program is free software; any publications presenting results
 * obtained with this program must mention it and its origin. You 
 * can redistribute it and/or modify it under the terms of the GNU 
 * General Public License as published by the Free Software 
 * Foundation; either version 2 of the License, or (at your option) 
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
*/
#include "ann.h"


/**
 * Translates 3d coord into a 1d array coord
*/
uint8_t as3D(uint8_t x, uint8_t y, uint8_t z) {
    return x + NETWORK_WIDTH * (y + NETWORK_DEPTH * z);
}

/**
 * This function will initalize the
 * neural network.
 * double * network - The weights of the network
*/
void ann_init_network(double * network) {
    uint8_t i;
    for(i=0; i<GENOME_LENGTH; i++) {
        network[i] = (double)rand() / (double)RAND_MAX;
    }
}

/**
 * This function is used to
 * calculate the ouput of the neural
 * network.
 * double * network - The weights of the network
 * double * input   - The input
 * double * output  - The output
*/
void ann_calculate( double * network, double * inputRaw, double * output ) {
    uint8_t x,y,z;
    double * input = (double *)calloc( NETWORK_HEIGHT, sizeof(double) );
    memcpy( input, inputRaw, NETWORK_HEIGHT * sizeof(double) );

    for(x=0; x<NETWORK_WIDTH; x++) {
        memset( output, 0, NETWORK_HEIGHT * sizeof(double) );
        for(y=0;y<NETWORK_HEIGHT; y++) {
            for(z=0; z<NETWORK_DEPTH; z++) {
                if(DEBUG==1){
                    printf("outN[%d](%f) += inN[%d](%f) * weight(%f)\n", z, output[z], y, input[y], network[ as3D(x,y,z) ]);
                }
                output[z] += input[y] * network[ as3D(x,y,z) ];
            }
        }
        for(y=0;y<NETWORK_HEIGHT; y++) {
            if(DEBUG==0){
                output[y] = (output[y] / (1 + abs(output[y]))); /*Fast sigmoid*/
            }
        }
        if( x < NETWORK_WIDTH-1 ){
            memcpy( input, output, NETWORK_HEIGHT * sizeof(double) );
        }
    }
    free(input);
}

/**
 * Print the artificial neural network
 * double * network - The weights of the network
*/
void ann_print( double * network ) {
    uint8_t x,y,z;
    for(x=0; x<NETWORK_WIDTH; x++) {
        for(y=0;y<NETWORK_HEIGHT; y++) {
            for(z=0; z<NETWORK_DEPTH; z++) {
                printf( "%f\n", network[ as3D(x,y,z) ] );
            }
        }
    }
}