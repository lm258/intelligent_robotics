/***************************************************************************
  
  e-puck_line -- Base code for a practical assignment on behaviour-based
  robotics. When completed, the behaviour-based controller should allow 
  the e-puck robot to follow the black line, avoid obstacles and
  recover its path afterwards.
  Copyright (C) 2006 Laboratory of Intelligent Systems (LIS), EPFL
  Authors: Jean-Christophe Zufferey
  Email: jean-christophe.zufferey@epfl.ch
  Web: http://lis.epfl.ch

  This program is free software; any publications presenting results
  obtained with this program must mention it and its origin. You 
  can redistribute it and/or modify it under the terms of the GNU 
  General Public License as published by the Free Software 
  Foundation; either version 2 of the License, or (at your option) 
  any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
  USA.

***************************************************************************/
#include <webots/gps.h>
#include <webots/robot.h>
#include <webots/emitter.h>
#include <webots/differential_wheels.h>
#include <webots/distance_sensor.h>
#include <webots/light_sensor.h>
#include <webots/led.h>
#include <math.h>
#include <stdio.h>
#include <time.h>
#include <ann.h>
#include <webots/receiver.h>

// Global defines
#define LEFT 5
#define RIGHT 6
#define TIME_STEP  32
#define MAX_SPEED 400

// 8 IR proximity sensors
#define PS_MAX_VALUE 3000
#define PS_THRESHOLD 0.10 // threshhold for when to count sensor as activated
#define NB_DIST_SENS 8
WbDeviceTag ps[NB_DIST_SENS];	/* proximity sensors */

// 3 IR ground color sensors
#define NB_GROUND_SENS 3
#define GS_THRESHHOLD 0.8
#define GS_MAX_VALUE 1000
WbDeviceTag gs[NB_GROUND_SENS]; /* ground sensors */
unsigned short gs_value[NB_GROUND_SENS]={0,0,0};

/*Used for getting our last wheel speed*/
static double lws[2] = {0};
static int elapsed_steps  = 0;
static int sensor_steps   = {0}; //amount of steps when each sensor has been activated XOR not both
static double last_pos[3] = {0};

// for receiving genes & sending fitness from Supervisor
WbDeviceTag receiver;
WbDeviceTag emitter;

#define GPS_THRESHOLD 0.1f //how much should we move before it is counted as a move
WbDeviceTag gps;

/*Create some little arrays to store our neural network in*/
double weights[GENOME_LENGTH] = {0};
double input[NETWORK_HEIGHT]  = {0};
double output[NETWORK_HEIGHT] = {0};

// check if a new set of genes was sent by the Supervisor
// in this case start using these new genes immediately
void check_for_new_genes() {
  if (wb_receiver_get_queue_length(receiver) > 0) {
    memcpy(weights, wb_receiver_get_data(receiver), GENOME_LENGTH * sizeof(double));
    wb_receiver_next_packet(receiver);
  }
}

//This function sends our fitness to the supervisor
void send_calculated_fitness(){
  double fitness = (double)(sensor_steps)/(float)elapsed_steps;
  wb_emitter_send(emitter, &fitness, sizeof(double));
  sensor_steps  = 0;
  elapsed_steps = 0;
}

//------------------------------------------------------------------------------
//
//    CONTROLLER
//
//------------------------------------------------------------------------------
////////////////////////////////////////////
// Main
int main()
{
  int i,j, gs_sense, ps_sense, mv_sense, gs_average, d_sense;
  gs_sense = ps_sense = mv_sense = d_sense = 0;

  /* intialize Webots */
  wb_robot_init();

  /* initialization */
  char name[20];
  for (i = 0; i < NB_DIST_SENS; i++) {
    sprintf(name, "ps%d", i);
    ps[i] = wb_robot_get_device(name); /* proximity sensors */
    wb_distance_sensor_enable(ps[i],TIME_STEP);
  }
  for (i = 0; i < NB_GROUND_SENS; i++) {
    sprintf(name, "gs%d", i);
    gs[i] = wb_robot_get_device(name); /* ground sensors */
    wb_distance_sensor_enable(gs[i],TIME_STEP);
  }

  // receiver to get genome from supervisor
  receiver = wb_robot_get_device("receiver");
  wb_receiver_enable(receiver, TIME_STEP);

  // the emitter to send fitness to supervisor
  emitter = wb_robot_get_device("emitter");
  wb_emitter_set_range(emitter, -1);
  wb_emitter_set_channel(emitter, 2);

  // enable gps so we can tgrack position
  gps = wb_robot_get_device("gps"); 
  wb_gps_enable(gps, TIME_STEP);

  while(wb_robot_step(TIME_STEP) != -1)
  {
    // reset the check of wether the sensors are activated or not
    gs_sense = ps_sense = mv_sense = 0;

    if( elapsed_steps == ((1000.0*60)/TIME_STEP)+1 ) {
      send_calculated_fitness();
    }

    /*Check for our new genes*/
    check_for_new_genes();

    /*Network has 13 inputs*/
    /*Map Ground Sensors into the middle of the network 5,6,7*/
    for(i=gs_average=0;i<NB_GROUND_SENS;i++) {
      unsigned short value = wb_distance_sensor_get_value(gs[i]);
      gs_average += value;
      input[i+5] = (double)value/(double)GS_MAX_VALUE;
    }
    gs_average = gs_average/NB_GROUND_SENS;
    if( gs_average < (GS_MAX_VALUE * GS_THRESHHOLD) ){
      gs_sense = 1;
    }

    /*Map the Last weel speed to 4,8*/
    input[4] = lws[0];
    input[8] = lws[1];

    /*Map pressure sensors to 0,1,2,3 and 9,10,11,12*/
    for(i=0; i<NB_DIST_SENS; i++) {
      double value = wb_distance_sensor_get_value(ps[i]);
      if( value < 0 ) {
        value = 0;
      }
      if( value > (PS_THRESHOLD*PS_MAX_VALUE) ){
        ps_sense = 1;
      }
      j = (i<4) ? i : i+5;
      input[j] = value/(double)(PS_MAX_VALUE);
    }

    /*check our last position against current to see if we moved*/
    const double * cur_pos = wb_gps_get_values(gps);
    if( fabs(cur_pos[0]-last_pos[0])*1000.0f > GPS_THRESHOLD || fabs(cur_pos[2]-last_pos[2])*1000.0f > GPS_THRESHOLD ){
      mv_sense = 1;
    }
    memcpy( last_pos, cur_pos, sizeof(double) * 3);

    if( sqrt((cur_pos[0]*cur_pos[0]) + (cur_pos[2]*cur_pos[2])) < 3.3 ) {
      d_sense = 1;
    } else {
      sensor_steps -= 10;
    }

    /*Calculate our sensor step for fitness function*/
    if( (gs_sense == 1 || ps_sense == 1) && ( ps_sense != gs_sense) && mv_sense == 1 && d_sense == 1 ){
      sensor_steps++;
    }

    /*Calculate the network and store it in the output*/
    ann_calculate( weights, input, output );

    /*Set our last wheel speed*/
    lws[0] = output[LEFT];
    lws[1] = output[RIGHT];

    /*Set wheel speeds, to the leftur and right position in our output*/
    wb_differential_wheels_set_speed(output[LEFT] * MAX_SPEED, output[RIGHT] * MAX_SPEED);

    /*increment step*/
    elapsed_steps++;
  }
  return 0;
}
