#!/bin/bash
WEBOTS_HOME_DIR_PG="../../../../../webots"
WEBOTS_HOME_DIR="../../../../webots"
make -sC lib/ -f Makefile WEBOTS_HOME=$WEBOTS_HOME_DIR $1
make -sC smart-puck/ -f altmake WEBOTS_HOME=$WEBOTS_HOME_DIR $1
make -sC supervisor/ -f altmake WEBOTS_HOME=$WEBOTS_HOME_DIR $1