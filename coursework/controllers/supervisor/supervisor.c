//   File:          advanced_genetic_algorithm_supervisor.c
//   Description:   Supervisor code for genetic algorithm
//   Project:       Advanced exercises in Cyberbotics' Robot Curriculum
//   Author:        Yvan Bourquin - www.cyberbotics.com
//   Date:          January 6, 2010
#include <webots/supervisor.h>
#include <webots/receiver.h>
#include <webots/robot.h>
#include <webots/emitter.h>
#include <webots/display.h>
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <ann.h>
#include <ea.h>

#define LOAD_FITNESS_FILE "fitness.text"

static int demo = false;
static int time_step;
static WbDeviceTag emitter;   // to send genes to robot
static WbDeviceTag receiver;   // to receive fitness from GUI

// for reading or setting the robot's position and orientation
static WbFieldRef robot_translation;
static WbFieldRef robot_rotation;
static double robot_trans0[3];  // a translation needs 3 doubles
static double robot_rot0[4];    // a rotation needs 4 doubles
static double best_fitness;
static FILE*  FITNESS_FILE;
static double weights_test[GENOME_LENGTH] = {0.920139,0.078131,0.029241,0.402116,0.394599,0.914982,0.821185,0.070509,0.477997,0.009408,0.637317,0.830999,0.790548,0.070783,0.852635,0.040943,0.806721,0.754736,0.618939,0.653740,0.690626,0.140746,0.508117,0.543648,0.671769,0.103897,0.267759,0.778519,0.196952,0.725977,0.616513,0.882707,0.804108,0.645754,0.519208,-0.103081,0.560736,0.340392,0.269216,0.038733,0.349801,0.906532,0.869732,0.140349,0.977316,0.722367,0.181291,0.784037,1.904685,0.800230,0.437777,0.167730,0.940977,0.945894,0.711378,0.612745,0.049791,0.979137,0.391264,0.246743,0.705114,0.007778,0.363835,1.773947,0.653532,0.883043,0.707928,0.214267,1.800452,0.977144,0.253000,0.573236,0.883676,0.122733,0.713584,0.860992,0.845100,0.296981,-0.529064,0.322203,0.695106,0.082806,0.489933,0.636083,0.028700,0.201311,0.248828,0.078491,0.180448,0.640093,1.388791,0.885561,0.647871,0.689069,0.394783,0.301402,0.572112,0.102711,0.515670,0.795547,0.079854,0.768670,0.368783,0.963531,0.891403,0.321871,0.824523,0.736503,0.977243,0.469552,0.058706,0.672349,0.552358,0.548639,0.308432,0.581058,0.749950,0.557260,0.659549,0.930398,0.197353,0.984784,0.815960,1.866972,0.673854,0.210742,0.146626,0.245966,0.313453,0.662296,0.041513,0.393307,0.430966,0.410296,0.356838,0.322368,0.492663,0.181361,0.058871,0.469906,0.650913,0.117577,0.142255,0.203271,0.666216,0.450687,0.784329,0.416167,0.007947,0.443878,0.346565,-1.732350,0.428662,0.162524,0.050524,0.102516,0.373267,0.197150,0.348482,0.686719,0.859446,0.389994,0.080027,0.290412,0.800290,0.436865,0.612780,0.292953,0.618225};

/*Run for x seconds*/
void run_seconds(double seconds) {
  int i, n = 1000.0 * seconds / time_step;
  for (i = 0; i < n; i++) {
    if (demo && wb_robot_keyboard_get_key() == 'O') {
      demo = false;
      return; // interrupt demo and start GA optimization
    }
    wb_robot_step(time_step);
  }
}

/**
 * Evaluate fitness of this indivual set of weights
 * struct * Individual
 * returns double Fitness of individual
*/
double ea_evaluate_individual_fitness(Individual * individual) {

  // send genotype to robot for evaluation
  wb_emitter_send(emitter, weights_test/*individual->genome*/, GENOME_LENGTH * sizeof(double));
  
  // reset robot position
  wb_supervisor_simulation_reset_physics();
  wb_supervisor_field_set_sf_vec3f(robot_translation, robot_trans0);
  wb_supervisor_field_set_sf_rotation(robot_rotation, robot_rot0);

  // evaluation genotype during one minute
  run_seconds(60.0);

  /*Wait for robot to send fitness*/
  while(wb_receiver_get_queue_length(receiver) == 0) {
    wb_robot_step(time_step);
  }
  double fitness;
  memcpy(&fitness, wb_receiver_get_data(receiver), sizeof(double));
  printf("fitness: %f\n", fitness);
  wb_receiver_next_packet(receiver);

  /*Save personal best into file, also store the current best*/
  if( individual->fitness > best_fitness ) {
      ea_fprint_genotype( individual, FITNESS_FILE );
      best_fitness = individual->fitness;
  }

  /*Write this individual to the file*/
  return fitness;
}

int main(int argc, const char *argv[]) {

  //seed random
  time_t seed = time(NULL);
  srand(seed);

  // open fitness file
  int i = rand() % 100;
  char file_name[56];
  sprintf( file_name, "fittest-%d.txt", i );
  FITNESS_FILE = fopen( file_name, "a");

  /*Print the seed*/
  fprintf( FITNESS_FILE, "SEED: %d\n", (int)seed);
  
  // initialize Webots
  wb_robot_init();
  
  // get simulation step in milliseconds
  time_step = wb_robot_get_basic_time_step();
  wb_robot_keyboard_enable( time_step );

  // the emitter to send genotype to robot
  emitter = wb_robot_get_device("emitter");
  wb_emitter_set_range(emitter, -1);

  // create receiver to get fitness from GUI
  receiver = wb_robot_get_device("receiver");
  wb_receiver_enable(receiver, time_step);

  /*Find robot node and store initial position and orientation*/
  WbNodeRef robot   = wb_supervisor_node_get_from_def("ROBOT");
  robot_translation = wb_supervisor_node_get_field(robot, "translation");
  robot_rotation    = wb_supervisor_node_get_field(robot, "rotation");
  memcpy(robot_trans0, wb_supervisor_field_get_sf_vec3f(robot_translation), sizeof(robot_trans0));
  memcpy(robot_rot0, wb_supervisor_field_get_sf_rotation(robot_rotation), sizeof(robot_rot0));

  /*Create inital population*/
  ea_create_population();

  /*run GA optimization*/
  int generations       = 1000;
  double mutation_rate  = 0.9;
  double crossover_rate = 0.9;
  ea_evolve(generations, mutation_rate, crossover_rate);
  
  /*Clean up*/
  wb_robot_cleanup();
  fclose(FITNESS_FILE);
  return 0;
}

